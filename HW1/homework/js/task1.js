const MIN_INITIAL_AMOUNT = 1000;
const MIN_NUMBER_OF_YEARS = 1;
const MAX_PERCENTAGE_OF_YEAR = 100;

function calculateProfit(initialAmount, numberOfYears, percentage) {
  let amount = initialAmount;
  for (let i = 0; i < numberOfYears; i += 1) {
    amount *= 1 + percentage / 100;
  }
  return amount;
}

const initialAmount = parseFloat(prompt("Initial amount:"));
const numberOfYears = parseFloat(prompt("Number of years:"));
const percentage = parseFloat(prompt("Percentage of year:"));

const totalAmount = calculateProfit(initialAmount, numberOfYears, percentage);

if (
  initialAmount >= MIN_INITIAL_AMOUNT &&
  numberOfYears >= MIN_NUMBER_OF_YEARS &&
  percentage <= MAX_PERCENTAGE_OF_YEAR &&
  Number.isInteger(numberOfYears)
) {
  alert(`Initial amount: ${initialAmount}
    Number of years: ${numberOfYears}
    Percentage of year: ${percentage}
    
    Total profit: ${(totalAmount - initialAmount).toFixed(2)}
    Total amount: ${totalAmount.toFixed(2)}`);
} else alert("Invalid input data");
