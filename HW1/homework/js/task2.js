function startGame(maxPocketNumber, totalPrize, posiblePrize) {
  const currentPrize = gameProcess(maxPocketNumber, totalPrize, posiblePrize);
  if (currentPrize) {
    totalPrize += currentPrize;
    if (confirm(`Congratulation, you won! Your prize is: ${totalPrize} $. Do you want to continue?`)) {
      startGame(maxPocketNumber + 4, totalPrize, posiblePrize * 2);
    } else {
      endGame(currentPrize);
    }
  } else {
    endGame(currentPrize);
  }
}

function gameProcess(maxPocketNumber, totalPrize, posiblePrize) {
  const winningNumber = Math.floor(Math.random() * (maxPocketNumber + 1));
  for (let i = 3; i > 0; i--) {
    const userNumber = parseFloat(
      prompt(`Choose a roulette pocket number from 0 to ${maxPocketNumber}
          Attempts left: ${i}
          Total prize: ${totalPrize}$
          Possible prize on current attempt: ${posiblePrize}$`)
    );
    if (userNumber == winningNumber) {
      return posiblePrize;
    } else {
      posiblePrize /= 2;
    }
  }
  return 0;
}

function endGame(currentPrize) {
  alert(`Thank you for your participation. Your prize is: ${currentPrize} $`);
  if (confirm("Do you want to play again?")) {
    startGame(8, 0, 100);
  } else {
    alert("You did not become a billionaire, but can");
  }
}

confirm("Do you want to play a game?") ? startGame(8, 0, 100) : alert("You did not become a billionaire, but can");
