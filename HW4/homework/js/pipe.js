const pipe = (value , ...functions) => {
    let position = 0;
    for (const fn of functions) {
        position++;
        if (typeof fn !== 'function') {
            throw new Error(`Provided argument at position ${position} is not a function!`);
          }
        value = fn(value);
    }
    return value;
}