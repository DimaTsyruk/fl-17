function visitLink(page) {
    let count1 = localStorage.getItem('page1');
    let count2 = localStorage.getItem('page2');
    let count3 = localStorage.getItem('page3');
    if (page == 'Page1') {
        localStorage.setItem('page1', ++count1);
    }
    else if (page == 'Page2') {
        localStorage.setItem('page2', ++count2);
    }
    else {
        localStorage.setItem('page3', ++count3);
    }
}

function viewResults() {
    if(localStorage.getItem('page1')===null) { localStorage.setItem('page1', 0); } 
    if(localStorage.getItem('page2')===null) { localStorage.setItem('page2', 0); } 
    if(localStorage.getItem('page3')===null) { localStorage.setItem('page3', 0); } 
    const button = document.querySelector('button');
    const text = document.createElement('p');
    text.innerHTML = `<ul>
    <li>You visited Page1 ${localStorage.getItem('page1')} time(s)</li>
    <li>You visited Page2 ${localStorage.getItem('page2')} time(s)</li>
    <li>You visited Page3 ${localStorage.getItem('page3')} time(s)</li>
  </ul>`;
    button.parentNode.insertBefore(text, button.nextSibling); 
    localStorage.clear();
}

