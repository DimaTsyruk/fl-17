//task 1
function getAge(birthDate){
    const today = new Date()
    if (today >= new Date(today.getFullYear(), birthDate.getMonth(), birthDate.getDate())) {
        return today.getFullYear() - birthDate.getFullYear();
    } else {
        return today.getFullYear() - birthDate.getFullYear() -1;
    }
}

//task 2
function getWeekDay(date){
    const day = new Date(date).getDay()
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return weekdays[day];
}

//task 3
function getAmountDaysToNewYear(){
    const today = new Date();
    const newYear = new Date(today.getFullYear() + 1, 0, 1);
    const daysToNewYear = Math.ceil((newYear.getTime() - today.getTime()) / (1000 * 60 * 60 *24))
    return daysToNewYear;
}

//task 4
function getProgrammersDay(year) {
    const date = new Date(year, 0, 1)
    const programmersDay = new Date(date.getTime() + 255 * 24 * 60 * 60 * 1000)
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const dayOfWeek = weekdays[programmersDay.getDay()];
    const month = months[programmersDay.getMonth()];
    const dayOfMonth = programmersDay.getDate();
    return `${dayOfMonth} ${month}, ${year} (${dayOfWeek})`;
}

//task 5
function howFarIs(nameOfDay){
    const today = new Date()
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    nameOfDay = nameOfDay[0].toUpperCase() + nameOfDay.slice(1).toLowerCase(); 
    const dayNumber = weekdays.indexOf(nameOfDay );
    const number = (7 + dayNumber - today.getDay()) % 7
    if (number == 0){
        return `Hey, today is ${ weekdays[today.getDay()]} =)`
    } else{
        return `It's ${ number } day(s) left till ${ weekdays[dayNumber] }`
    }

}


//task 6
function isValidIdentifier(str) {
    const regexp = /^[a-zA-Z_$][a-zA-Z0-9_$]*$/;
    return regexp.test(str) ;
}

//task 7
function capitalize(str) {
    const regexp = /\b\w/g;
    return str.replace(regexp, (el) => el.toUpperCase());
}

//task 8
function isValidAudioFile(str){
    const regexp = /^[a-zA-Z]+\.((mp3)|(flac)|(alac)|(aac))$/;
    return regexp.test(str);
}

//task 9
function getHexadecimalColors(str) {
    const regex = /(^|\s)#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})\b/g;
    const arr = str.match(regex);
    return arr || [];
}

//task 10
function isValidPassword(str){
    const regexp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d]{8,}$/;
    return regexp.test(str);
}

//task 11
function addThousandsSeparators(num){
    const regexp = /(?=(\d{3})+(?!\d))/g;
    return num.toString().replace(regexp, ",");
}

//task 12
function getAllUrlsFromText(text){
    const regex = /((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
    const arr = text.match(regex);
    return arr || [];
}


//calling functions
const birthday22 = new Date(2000, 9, 22);   //task 1
const birthday23 = new Date(2000, 9, 23);   //task 1
console.log(getAge(birthday22)); // 22   //task 1
console.log(getAge(birthday23)); // 22   //task 1
console.log(getWeekDay(Date.now())); //today of the week   //task2
console.log(getWeekDay(new Date(2020, 9, 22))); // "Thursday"   //task2
console.log(getAmountDaysToNewYear()); // number of days until the New Year   //task 3
console.log(getProgrammersDay(2020)); // "12 Sep, 2020 (Saturday)"   //task 4
console.log(getProgrammersDay(2019)); // "13 Sep, 2019 (Friday)"   //task4
console.log(howFarIs('friday')); //  returns the number of days to the next specified weekday   //task 5
console.log(howFarIs('Thursday')); //  returns the number of days to the next specified weekday   //task 5
console.log(isValidIdentifier('myVar!')); // false   //task 6 
console.log(isValidIdentifier('myVar$')); // true   //task 6
console.log(isValidIdentifier('myVar_1')); // true   //task 6
console.log(isValidIdentifier('1_myVar')); // false   //task 6
const testStr = "My name is John Smith. I am 27.";   //task 7
console.log(capitalize(testStr)); // "My Name Is John Smith. I Am 27."   //task 7
console.log(isValidAudioFile('file.mp4')); // false   //task8
console.log(isValidAudioFile('my_file.mp3')); // false   //task8
console.log(isValidAudioFile('file.mp3')); // true   //task8
const testString = "color: #3f3; background-color: #AA00ef; and: #abcd";
console.log(getHexadecimalColors(testString)); // ["#3f3", "#AA00ef"]   //task 9
console.log(getHexadecimalColors('red and #0000')); // [];   //task 9
console.log(isValidPassword('agent007')); // false (no uppercase letter)   //task 10
console.log(isValidPassword('AGENT007')); // false (no lowercase letter)   //task 10
console.log(isValidPassword('AgentOOO')); // false (no numbers)   //task 10
console.log(isValidPassword('Age_007')); // false (too short)   //task 10
console.log(isValidPassword('Agent007')); // true   //task 10
console.log(addThousandsSeparators("1234567890")); // "1,234,567,890"   //task 11
console.log(addThousandsSeparators(1234567890)); // "1,234,567,890"   //task 11
const text1 = "We use https://translate.google.com/ to translate some words and phrases from https://angular.io/";   //task 12
const text2 = "JavaScript is the best language for beginners!"   //task 12
console.log(getAllUrlsFromText(text1)); // ["https://translate.google.com/", "https://angular.io/"]   //task 12
console.log(getAllUrlsFromText(text2)); // []   //task 12
