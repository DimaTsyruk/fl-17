//task 1
function reverseNumber (num) {
    num = String(num);
    let res = '';
    if(num[0] == '-') {res += '-'; }
    let length=0;
    while(num[length] != undefined) { length++; }
    for( let i = length - 1; i > 0; i--){
        res += num[i];
    }
    if(num[0] != '-') {res += num[0]; }
    return res;
}

//task 2
function forEach (arr, func){
    for (let el of arr){
        func(el);
    }
}

//task 3
function map(arr, func){
    const res = [];
    forEach(arr, function(el) { return res.push(func(el)); })
    return res;
}

//task 4
function filter (arr, func) {
    const res = [];
    forEach(arr, function(el) { 
        if( func (el)){
            return res.push(el); 
        }
    })
    return res;
}

//task 5
function getAdultAppleLovers(data) {
    let res = filter(data, function(el) { return (el.age > 18 && el.favoriteFruit == "apple" ) })
    res = map (res, function(el) { return el.name })
    return res
   }

//task 6
function getKeys (obj) {
    const res =[];
    for (let key in obj ) {
        res.push(key)
    }
    return res
}

//task 7
function getValues (obj) {
    const res =[];
    for (let key in obj ) {
        res.push(obj[key])
    }
    return res
}

//task 8
function showFormattedDate (date){
    const newDate = String(date)
    const day = newDate[8] + newDate[9]
    const month  = newDate[4] + newDate[5]+newDate[6]
    const year  = newDate[11] + newDate[12]+newDate[13]+newDate[14]
    return `It is ${day} of ${month} , ${year}`
}

//function calls
/*
//task 1
console.log(reverseNumber(12345))  //  54321
console.log(reverseNumber(-56789)) //  -98765
//task 2
forEach([2,5,8], function(el) { console.log(el) }) // logs to console: 2 5 8
//task 3
console.log( map([2, 5, 8], function(el) { return el + 3; }) ) // returns [5, 8, 11]
console.log( map([1, 2, 3, 4, 5], function (el) { return el * 2; }) )  // returns [2, 4, 6, 8, 10]
//task 4
console.log( filter([2, 5, 1, 3, 8, 6], function(el) { return el > 3 }) ) // returns [5, 8, 6]
console.log( filter([1, 4, 6, 7, 8, 10], function(el) { return el % 2 === 0 }) ) //returns [4, 6, 8, 10]
// task 5
data = [
    {
        "_id": "5b5e3168c6bf40f2c1235cd6",
        "index": 0,
        "age": 39,
        "eyeColor": "green",
        "name": "Stein",
        "favoriteFruit": "apple"
    },
    {
        "_id": "5b5e3168e328c0d72e4f27d8",
        "index": 1,
        "age": 38,
        "eyeColor": "blue",
        "name": "Cortez",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "5b5e3168cc79132b631c666a",
        "index": 2,
        "age": 2,
        "eyeColor": "blue",
        "name": "Suzette",
        "favoriteFruit": "apple"
    },
    {
        "_id": "5b5e31682093adcc6cd0dde5",
        "index": 3,
        "age": 17,
        "eyeColor": "green",
        "name": "Weiss",
        "favoriteFruit": "banana"
    }
   ]
console.log( getAdultAppleLovers(data))  // returns ['Stein']
//task 6
console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3})) // returns ['keyOne', 'keyTwo', 'keyThree']
//task 7 
console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3})) // returns [1, 2, 3]
//task 8
console.log( showFormattedDate(new Date('2018-08-27T01:10:00')) ) // returns 'It is 27 of Aug, 2018'
*/