//task 1
function isEquals(firstArg, secondArg){
    return firstArg == secondArg
}

//task 2 
function isBigger(firstArg, secondArg){
    return firstArg > secondArg
}

//task 3
function storeNames(...names) {
    return names;
  }

//task 4 
function getDifference(firstArg, secondArg) {
    if(firstArg > secondArg) {return firstArg - secondArg}
    else{return secondArg - firstArg}
  }

//task 5
function negativeCount(arr) {
    let count = 0
    for (let el of arr) {
        if (el < 0) {count++}
    }
    return count
  }

//task 6
function letterCount(firstArg, secondArg) {
    let count = 0
    for (let el of firstArg) {
        if (el == secondArg) { count++ }
    }
    return count
}

//task 7
function countPoints(arr) {
    let points = 0
    for (let el of arr) {
        let result = el.split(":")
        if (parseInt(result[0]) > parseInt(result[1])) { points += 3}
        else if (parseInt(result[0]) == parseInt(result[1])) { points += 1}
    }
    return points
}

//calling functions
console.log(isEquals(3, 3)) // => true   //task 1
console.log(isBigger(5, -1)) // => true   //task 2
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy')) // => ['Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy']   //task 3
console.log(getDifference(5, 3) ) // => 2  //task 4
console.log(getDifference(5, 8) ) // => 2  //task 4
console.log(negativeCount([4, 3, 2, 9])) // => 0 //task 5
console.log(negativeCount([0, -3, 5, 7])) // => 1 //task 5
console.log (letterCount("Marry", "r")) // => 2  //task 6
console.log (letterCount("Barny", "y")) // => 1  //task 6
console.log (letterCount("", "z")) // => 0  //task 6\
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100'])) // => 17  //task7